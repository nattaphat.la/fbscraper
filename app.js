const puppeteer = require('puppeteer');
const CRED = require('./creds');
const csv = require("fast-csv");
const fs = require('fs');
const sleep = async (ms) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res();
    }, ms)
  });
}

const ID = {
  login: '#email',
  pass: '#pass'
};
const fbUser = 'Pakkaad Chan';

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox','--disable-notifications']
  });
  const page = await browser.newPage();
  await page.setViewport({ width: 1024, height: 1024 });
  let openFacebook = async () => {
    // login
    await page.goto('https://facebook.com', {
      waitUntil: 'networkidle2'
    });
  }
  let login = async () => {
    // login
    await page.goto('https://facebook.com', {
      waitUntil: 'networkidle2'
    });
    await page.waitForSelector(ID.login);
    console.log('logging in');
    await page.type(ID.login, CRED.user);

    await page.type(ID.pass, CRED.pass);
    await sleep(500);
    await page.hover('#loginbutton')
    await page.click("#loginbutton")

    console.log("login done");
  }
  let searchUser = async (user) => {
    console.log(`searching for user ${user}`);
    // await page.waitForNavigation();
    await page.waitForSelector('[name=q]');
    await page.focus('[name=q]')
    await page.evaluate(function() {
      document.querySelector('[name=q]').value = ''
    })
    await page.type('[name=q]', user);
    await sleep(1000);
    await page.keyboard.press('Enter');
    // await page.waitForSelector('._19bq');
    // await page.click('._19bq');
    // await page.waitForNavigation({
    //   waitUntil: 'networkidle2',
    // })
    await sleep(1000);
    await page.waitForSelector('._5bcu', {timeout: 10000});
    await page.hover('._5bcu')
    await page.click('._5bcu');
    // await sleep(1000);

  }
  let gotoAboutTab = async () => {
    await page.waitForSelector('[data-tab-key=about]')
    console.log(`opening about tab`);
    await page.hover('[data-tab-key=about]')
    await page.click('[data-tab-key=about]')
    // await page.waitForNavigation({
    //   waitUntil: 'networkidle2',
    // })
  }
  let gotoAboutTabChild = async (tab, name) => {
    await page.waitForSelector(`[testid=${tab}]`, {timeout: 5000})
    console.log(`opening ${name || tab} tab`);
    await page.hover(`[testid=${tab}]`)
    await page.click(`[testid=${tab}]`)
  }
  let getWorkExperience = async (user) => {
    try {
      await page.waitForSelector('._4qm1 [role=heading]', {timeout: 3000});
      const label = await page.$eval('._4qm1 [role=heading]', e => e.innerHTML);
      if (label != 'ที่ทำงาน') {
        return false;
      }
      await page.waitForSelector('._4qm1 .experience ._2lzr a', {timeout: 3000})
      console.log(`Searching for working experience`);
      const workExperiences = await page.$$eval('._4qm1 .experience ._2lzr a', e => e.map(o => o.innerHTML));
      console.log(`${user} working experience`, workExperiences);
      return workExperiences
    } catch (e) {
      return false;
    }
  }
  let getEducationExperience = async (user) => {
    try {
      await page.waitForSelector('._4qm1 [role=heading]', {timeout: 3000});
      const label = await page.$eval('._4qm1 [role=heading]', e => e.innerHTML);
      if (label != 'การศึกษา') {
        return false;
      }
      await page.waitForSelector('._4qm1 .experience ._2lzr a', {timeout: 3000})
      console.log(`Searching for education experience`);
      const educationExperiences = await page.$$eval('._4qm1 .experience ._2lzr a', e => e.map(o => o.innerHTML));
      console.log(`${user} education experience`, educationExperiences);
      return educationExperiences
    } catch (e) {
      return false;
    }
  }
  let getGender = async (user) => {
    try {
      await page.waitForSelector('._3ms8 ._pt5 span', {timeout: 3000})
      console.log(`Searching for gender`);
      const data = await page.$eval('._3ms8 ._pt5 span', e => e.innerHTML);
      console.log(`${user} gender`, data);
      return data;
    } catch(e) {
      return false;
    }
  }
  let getPlaces = async (user) => {
    try {
      await page.waitForSelector('._2iel a', {timeout: 3000})
      console.log(`Searching for all city`);
      const cities = await page.$$eval('._2iel a', e => e.map(o => o.innerHTML));
      console.log(`${user} city`, cities);
      return cities
    } catch (e) {
      return false;
    }
  }
  let getCurrentPlaces = async (user) => {
    try {
      await page.waitForSelector('#current_city ._2iel a', {timeout: 3000})
      console.log(`Searching for current city`);
      const currentCity = await page.$eval('#current_city ._2iel a', e => e.innerHTML);
      console.log(`${user} current city`, currentCity);
      return currentCity
    } catch(e) {
      return false;
    }
  }
  let getMartialStatus = async (user) => {
    try {
      await page.waitForSelector('._4qm1 ._2ieq', {timeout: 3000})
      console.log(`Searching for martial status`);
      const martialStatus = await page.$eval('._4qm1 ._2ieq', e => e.innerHTML);
      console.log(`${user} martial status`, martialStatus);
      return martialStatus
    } catch(e) {
      try {
        await page.waitForSelector('._4qm1 ._50f5', {timeout: 3000})
        console.log(`Searching for martial status`);
        const martialStatus = await page.$eval('._4qm1 ._50f5', e => e.innerHTML);
        console.log(`${user} martial status`, martialStatus);
        return martialStatus
      } catch (e2) {
        return false;
      }
    }
  }
  let getLikes = async (user) => {
    try {
      console.log('Searching more tab');
      await page.waitForSelector('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a', {timeout: 3000})
      console.log('Click more tab');
      await page.hover('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a');
      await page.click('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a');
      await page.waitForSelector('[data-tab-key=likes]', {timeout: 30000})
      console.log(`opening like tab`);
      await page.hover('[data-tab-key=likes]')
      await page.click('[data-tab-key=likes]')
      await page.evaluate(() => {
        function sleep(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        }
        function run() {
          return new Promise(async resolve => {
            console.log('start scrolling')
            await sleep(1000);
            console.log(document.getElementsByClassName('_359'));
            while (document.getElementsByClassName('_359').length > 0) {
              document.getElementsByClassName('_359')[0].scrollIntoView(true);
              await sleep(1000);
              console.log('scrolling');
            }
            resolve();
          });
        }
        return run();
      })
      console.log('scroll done');
      await page.waitForSelector('.fwb a');
      const likes = await page.$$eval('.fwb a', e => e.length);
      return likes;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
  let gotoLikeTab = async () => {
    try {
      console.log('Searching more tab');
      await page.waitForSelector('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a', {timeout: 3000})
      console.log('Click more tab');
      await page.hover('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a');
      await page.click('#fbTimelineHeadline [data-referrer="timeline_light_nav_top"] li:last-child a');
      await page.waitForSelector('[data-tab-key=likes]', {timeout: 10000})
      console.log(`opening like tab`);
      await page.hover('[data-tab-key=likes]')
      await page.click('[data-tab-key=likes]')
      // await sleep(1000);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
  let scrollToEnd = async () => {
    try {
      console.log('scroll start');
      await page.evaluate(() => {
        function sleep(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        }
        function run() {
          return new Promise(async resolve => {
            console.log('start scrolling')
            await sleep(1000);
            while (document.querySelectorAll("#pagelet_timeline_medley_likes ._359").length > 0) {
              document.querySelector("#pagelet_timeline_medley_likes").scrollIntoView({
                // behavior: 'smooth',
                block: 'end'
              });
              await sleep(1000);
              console.log('scrolling');
              if (document.querySelectorAll("#pagelet_timeline_medley_likes .hidden_elem ._359").length > 0) break;
            }
            resolve();
          });
        }
        return run();
      })
      console.log('scroll done');
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
  let changeLikeTab = async (tabName) => {
    try {
      console.log(`Searching for ${tabName} tab`);
      await page.waitForSelector(`[role=tab][name="${tabName}"]`, {timeout: 3000})
      await page.evaluate((tab) => {
        document.querySelector(`[role=tab][name="${tab}"]`).scrollIntoView({
          block: 'center',
          inline: 'center',
        });
        return new Promise(resolve => resolve());
      }, tabName)
      console.log(`Click on ${tabName} tab`);
      await page.hover(`[role=tab][name="${tabName}"]`);
      await page.click(`[role=tab][name="${tabName}"]`);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
  let countLike = async (tabName) => {
    try {
      const isChangeTab = await changeLikeTab(tabName);
      if (!isChangeTab) {
        return 0;
      }
      // await sleep(1000);
      const isScrollToend = await scrollToEnd();
      if (!isScrollToend) {
        return 0;
      }
      await page.waitForSelector('._30f[aria-role=tabpanel]:not(.hidden_elem) .fwb a', {timeout: 3000});
      const likes = await page.$$eval('._30f[aria-role=tabpanel]:not(.hidden_elem) .fwb a', e => e.length);
      return likes;
    } catch (e) {
      console.log(e);
      return 0;
    }
  }
  let getUserInfo = async (user) => {
    // await openFacebook();
    try {
      await searchUser(user);
      await gotoAboutTab();
      await gotoAboutTabChild('nav_edu_work', 'Education and work')
      const workExperiences = await getWorkExperience(user);
      const educationExperiences = await getEducationExperience(user);
      await gotoAboutTabChild('nav_contact_basic', 'Basic contact')
      const gender = await getGender(user);
      await gotoAboutTabChild('nav_places', 'Places');
      const places = await getPlaces(user);
      const currentPlace = await getCurrentPlaces(user);
      await gotoAboutTabChild('nav_all_relationships', 'Family and relationship');
      const martialStatus = await getMartialStatus(user);
      // const likes = await getLikes(fbUser); 
      const hasLikeTab = await gotoLikeTab();
      let movieCount = 0,
        tvCount = 0,
        musicCount = 0,
        bookCount = 0,
        sportTeamCount = 0,
        athleteCount = 0,
        restaurantCount = 0,
        appCount = 0;
      if (hasLikeTab) {
        // await sleep(1000);
        movieCount = await countLike('ภาพยนตร์')
        // await sleep(1000);
        tvCount = await countLike('รายการโทรทัศน์')
        // await sleep(1000);
        musicCount = await countLike('เพลง')
        // await sleep(1000);
        bookCount = await countLike('หนังสือ')
        // await sleep(1000);
        sportTeamCount = await countLike('ทีมกีฬา')
        // await sleep(1000);
        athleteCount = await countLike('นักกีฬา')
        // await sleep(1000);
        restaurantCount = await countLike('ร้านอาหาร')
        // await sleep(1000);
        appCount =  await countLike('แอพและเกม')
      }
      const userInfo = {
        workExperiences,
        educationExperiences,
        gender,
        places,
        currentPlace,
        martialStatus,
        movieCount,
        tvCount,
        musicCount,
        bookCount,
        sportTeamCount,
        athleteCount,
        restaurantCount,
        appCount,
      }
      console.log(`User ${user} info`, userInfo)
      return Promise.resolve(userInfo);
    } catch (e) {
      return Promise.reject(e);
    }
  }

  await login(fbUser);
  // await getUserInfo(fbUser);
  var csvStream = csv.createWriteStream({headers: true}),
    writableStream = fs.createWriteStream("output.csv");
  var stream = fs.createReadStream("UserDatabase.csv");
  let row = 0;
  csvStream.pipe(writableStream);
  try {
    csv
    .fromStream(stream, {headers : true})
    .transform(function(data, next){
      row = row+1;
      if (row < 7000) {
        return next();
      }
      getUserInfo(data['facebook_name'])
      .then((res) => {next(null, {facebook_id: data['facebook_id'], facebook_name: data['facebook_name'],...res})})
      .catch(e => {
        next()
      });
    })
    .on("data", async function(data){
      // await getUserInfo(data['facebook_name']);
      console.log(data);
      csvStream.write(data);
      // write to file
    })
    .on("end", function(){
        console.log("done");
        csvStream.end();
    });
  } catch (e) {
    csvStream.end();
  }
})();
